Kubeflow-Helm
=============

A central helm chart to make proof of concept deployment easy, and to be a base for integration into your own bespoke systems / kubernetes-based platforms.

I wanted a helm chart that I could easily install to both verify that kubeflow is the right tool, and to help me deploy it in production settings.

This helm chart will not cover everything, there will be things you will need to configure, but at the least this is a starting point.

Structure
---------

This helm package is split into two charts, one which is the dependency of the other. This is to do two things:

- I wanted to ensure the dependencies and CRDs are installed before any of the kubeflow manifests are even attempted.
- I wanted to make it easy to disable any and all of my modifications that may be opinionated away from the main kubeflow install.
