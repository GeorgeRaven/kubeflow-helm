CHART_DIR_PATH="charts/kubeflow"
CHART_NAME="kubeflow"
CHART_NAMESPACE="kubeflow"
MINIKUBE_KUBE_VERSION=1.27.2
PRIVATE_REGISTRY="registry.gitlab.com"
DOCKER_AUTH_FILE="${HOME}/.docker/config.json"
# https://docs.podman.io/en/latest/markdown/podman-login.1.html#authfile-path
REGISTRY_AUTH_FILE=${DOCKER_AUTH_FILE}

SRC_VERSION=$(shell git describe --abbrev=0)
APP_VERSION=$(shell grep -A1 "repository: docker.io/kubeflownotebookswg/centraldashboard" charts/kubeflow/values.yaml | grep -P -o '(?<=tag: ).*')
APP_VERSION=0.1.0

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@grep -F -h "##" $(MAKEFILE_LIST) | grep -F -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'


.PHONY: all
all: lint minikube install addons ## Create minikube cluster and apply operator to it

.PHONY: demo
demo: all ## (Re)Create minikube cluster and apply example CRDs to a fully working demo state
	@minikube addons list
	@echo THE IP OF YOUR MINIKUBE CLUSTER:
	@minikube ip
	@minikube dashboard

.PHONY: lint
lint: deps ## Lint the helm chart
	helm lint ${CHART_DIR_PATH}/.

.PHONY: deps
deps:	## Update all helm chart dependencies
	helm dependency update ${CHART_DIR_PATH}/.

.PHONY: minikube
minikube: ## Create a local minikube testing cluster
	minikube delete
	minikube start --cni calico --driver=podman --kubernetes-version=${MINIKUBE_KUBE_VERSION}

.PHONY: addons
addons: ## Enable our minikube required addons
	minikube addons enable ingress
	minikube addons enable metrics-server

.PHONY: login
login: login.lock

login.lock:
	# please use your username and a token with sufficient permissions to access the repo / registry
	sudo podman login ${PRIVATE_REGISTRY} --authfile ${REGISTRY_AUTH_FILE}
	sudo kubectl create -n ${REGCRED_NAMESPACE} secret generic ${REGCRED_NAME} --from-file=.dockerconfigjson=${REGISTRY_AUTH_FILE} --type=kubernetes.io/dockerconfigjson --dry-run=client -o yaml > login.creds
	# eval $(minikube docker-env)
	sudo podman logout ${PRIVATE_REGISTRY} --authfile ${REGISTRY_AUTH_FILE}
	# podman protects the registry file unlike docker. If it exists it will throw a permission error for other apps that expect it unpermed.
	sudo rm ${REGISTRY_AUTH_FILE}
	touch login.lock

.PHONY: template
template: templates.yaml ## Generate a concrete template for inspection

templates.yaml:
	helm template --set namespace.name=${CHART_NAMESPACE} --set namespace.create=true ${CHART_DIR_PATH}/. > templates.yaml

.PHONY: install-full
install-full: ## Install helm chart to default cluster with registry images
	helm dependency build ${CHART_DIR_PATH}
	helm upgrade --install --create-namespace --namespace ${CHART_NAMESPACE} ${CHART_NAME} ${CHART_DIR_PATH}/.

.PHONY: upgrade-full
upgrade-full: install-full ## Upgrade the operator helm chart using registry

.PHONY: install
install: ## Install helm chart to default cluster with local images
	helm dependency build ${CHART_DIR_PATH}
	helm upgrade --install --create-namespace --namespace ${CHART_NAMESPACE} ${CHART_NAME} ${CHART_DIR_PATH}/.

.PHONY: upgrade
upgrade: install

.PHONY: uninstall
uninstall: ## uninstall the operator helm chart
	helm uninstall --namespace ${CHART_NAMESPACE} ${CHART_NAME}
	# kubectl delete namespace ${CHART_NAMESPACE}

.PHONY: clean
clean: ## Wipe most residuals and clean up minikube
	rm -f login.lock login.creds templates.yaml
	minikube delete
	# podman logout {PRIVATE_REGISTRY}

.PHONY: stuck
stuck: ## Find any resources with finalizers that are blocking deletion
	kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n ${CHART_NAMESPACE}
